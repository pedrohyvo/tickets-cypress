describe("Tickets", () => { 
    beforeEach(() => cy.visit('/index.html'));
    
    it('fills all the text input fields', () => {
       const firstName = "Pedro";
       const lastName = "Andrade";

       cy.get('#first-name').type(firstName); 
       cy.get('#last-name').type(lastName); 
       cy.get('#email').type("example@gmail.com"); 
       cy.get('#ticket-quantity').select("2");
       cy.get('#vip').check();
       cy.get('#social-media').check();
       cy.get('#requests').type("QA Engineer");
       cy.get('#agree').click();
       cy.get('#signature').type(`${firstName} ${lastName}`);
       cy.get('button[type="submit"]').click();
       cy.get('.success').should('contain', 'Ticket(s) successfully ordered.').and('be.visible');
    });

    it("has 'TICKETBOX header's heading'", () => {
        cy.get('header h1').should('contain', 'TICKETBOX');
    });

    it('alerts on invalid email', () => {
        cy.get('#email')
          .as('email')
          .type('example-email.com');
        cy.get('#email.invalid').should('exist');

        cy.get('@email')
          .clear()
          .type('example@gmail.com');
          cy.get('#email.invalid').should('not.exist');
    });

    it('fills and reset the form', () => {
      const firstName = "Pedro";
      const lastName = "Andrade";
      const fullName = `${firstName} ${lastName}`;

      cy.get('#first-name').type(firstName); 
      cy.get('#last-name').type(lastName); 
      cy.get('#email').type("example@gmail.com"); 
      cy.get('#ticket-quantity').select("2");
      cy.get('#vip').check();
      cy.get('#friend').check();
      cy.get('#requests').type("QA Engineer");
      cy.get('.agreement p').should(
        "contain",
        `I, ${fullName}, wish to buy 2 VIP tickets.`
      );

      cy.get('#agree').click();
      cy.get('#signature').type(fullName);
      cy.get("button[type='submit']")
        .as('submitButton')
        .should('not.be.disabled');

      cy.get("button[type='reset']").click();
      cy.get('@submitButton').should('be.disabled');
    });

    it('fills mandatory fields using support command', () => {
      const customer = {
        firstName: "Brian",
        lastName: "Gold",
        email: "goldbrian@email.com"
      };

      cy.fillMandatoryFields(customer);

      cy.get("button[type='submit']")
        .as('submitButton')
        .should('not.be.disabled');

       cy.get("#agree").uncheck();
       cy.get('@submitButton').should('be.disabled');
    });
})